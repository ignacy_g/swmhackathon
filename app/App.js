import React from 'react';
import Map from './components/map';
import {/*Font,*/ AppLoading, Permissions, MapView} from 'expo';
import {StyleSheet, Text, View, TouchableOpacity, Dimensions, ScrollView} from 'react-native';
import data from './data';
import Nearby from "./components/nearby";

export default class App extends React.Component {

    render() {
        return (
            <View style={{flex: 1}}>
                <Map/>
                <Nearby/>
            </View>

        );
    }
}
