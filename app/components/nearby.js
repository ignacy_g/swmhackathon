import React from 'react';
import {Image, ScrollView, Text, View, StyleSheet, TouchableOpacity,TouchableHighlight, Modal} from "react-native";
import {Ionicons} from '@expo/vector-icons';

export default class Nearby extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            nearbyItems: [],
            modalVisible: false,
            element: null
        }
    }

    async componentDidMount() {
        await this.getNearbyItems();
    }

    async getNearbyItems() {
        this.setState({nearbyItems: require('../images.json')});
    }

    async getNearbyItem(id) {
        return this.state.nearbyItems[id - 1];
    }

    renderItemLocation = (element) => (
        <View>
            <Text style={styles.location}>{element.location}</Text>
            <Text style={styles.distance}>{element.distance}m</Text>
        </View>
    ); 

    renderItemStatus = (element) => (
        <View>
            <Text style={styles.added}>{element.time} min temu</Text>
            <View style={styles.status}><Ionicons name={'md-checkmark'} size={18}/></View>
        </View>
    );

    renderItemName = (element) => (
        <View style={styles.name}>
            <Text style={styles.nameText}>{element.name}</Text>
        </View>
    );

    renderList = () => (
        <View style={styles.nearby}>
            <View style={styles.bar}>
                <Text style={styles.title}>W POBLIŻU</Text>
                <Ionicons name={'ios-search'} size={32}/>
            </View>
            <ScrollView
                style={styles.itemList}
                keyboardShouldPersistTaps={"always"}
                keyboardDismissMode='on-drag'
            >
                {this.state.nearbyItems.map((element) => {
                    return this.renderListItem(element);
                })}
            </ScrollView>
        </View>

    );


    async showModal(element) {
        this.setState({element: element});
        this.setState({modalVisible: true});
    };

    renderListItem = (element) => {
        return (
            <TouchableOpacity
                onPress={() => {
                    this.showModal(element)
                }}
                style={styles.item} key={element.name}
            >
                <Image source={{uri: element.image}} style={{flex: 1, width: 100, height: 100}}/>
                <View style={{flex: 2, paddingLeft: 4}}>
                    {this.renderItemName(element)}
                    <View style={styles.info}>
                        {this.renderItemLocation(element)}
                        {this.renderItemStatus(element)}
                    </View>
                </View>
            </TouchableOpacity>
        )
    };

    renderModal = () => {
        if (this.state.element === null) return(<View/>);
        let { element } = this.state;
        console.log(element);
        return (
            <Modal
                onRequestClose={() => {this.setState(modalVisible = false)}}
                visible={this.state.modalVisible}
                transparent={true}
                style={{
                    paddingVertical: 50,
                    paddingHorizontal: 50
                }}
            >
                <View style={styles.modal}>
                    <View style={styles.modalView}>
                        <View style={styles.modalTitle}>
                            <Text>{element.name}</Text>
                        </View>
                        <TouchableHighlight
                onPress={() => {
                  this.setState({modalVisible:false});
                }}>
                <Text>Hide Modal</Text>
                </TouchableHighlight>
                    </View>
                </View>
            </Modal>
        );
    };

    render() {
        return (
            <View style={{flex: 1}}>
                {this.renderList()}
                {this.renderModal()}
            </View>
        );
    };
}

const styles = StyleSheet.create({
    nearby: {
        flex: 1,
        // backgroundColor: '#bfbfbf',
    },
    bar: {
        flexDirection: 'row',
        height: '15%',
        justifyContent: 'space-between',
        padding: 7,
        alignItems: 'center',
        backgroundColor: '#bfbfbfa1',
        borderTopLeftRadius: 12,
        borderTopRightRadius: 12,
    },
    info: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 10,
    },
    title: {
        fontSize: 25,
    },
    item: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 10,
        width: '100%',
        justifyContent: 'space-between'
    },
    name: {
        borderBottomColor: 'black',
        borderBottomWidth: 1
    },
    nameText: {
        fontWeight: 'bold',
        fontSize: 20,
    },
    location: {
        fontSize: 18
    },
    distance: {
        color: '#575757'
    },
    itemList: {
        backgroundColor: '#bfbfbf6a',
    },
    modal: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    modalView: {
        width: 300,
        height: 300,
        backgroundColor: '#ffd4aa',
        borderRadius: 20,
    },
    modalTitle: {
        alignItems: 'center'
    }
});