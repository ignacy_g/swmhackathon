import React from 'react';
import { Text, View, TouchableOpacity, StyleSheet, Dimensions, StatusBar, Image } from 'react-native';
import { Camera, Permissions } from 'expo';
import { Ionicons } from '@expo/vector-icons';

const sbHeight = StatusBar.currentHeight?StatusBar.currentHeight:16;

export default class CamWindow extends React.Component {
  state = {
    hasCameraPermission: null,
    photo: null,
  };

  async componentWillMount() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasCameraPermission: status === 'granted' });
  }

  snap = async () => {
    if (this.camera) {
      let photo = await this.camera.takePictureAsync({quality: 0.5, base64: true, exif: false});
      this.setState({photo: photo})
    }
  };

  getLocation = () => (<Text>Na Zjeździe 11 {'\n'} Kraków</Text>);

  render() {
    const { hasCameraPermission } = this.state;
    const { photo } = this.state;
    
    if (hasCameraPermission === null) {
      return <View />;
    } else if (hasCameraPermission === false) {
      return <Text>No access to camera</Text>;
    } else if (photo === null){
      return (
        <View>
            <View style={ camStyles.wrapper }>
                <Camera style={ camStyles.cam } ref={ref => { this.camera = ref; }} />
            </View>
            <View style={camStyles.bottomStuff}>
                <TouchableOpacity style={camStyles.bottomStuff} onPress={() => this.snap()}>
                    <Text style={camStyles.bottomLocation}>{this.getLocation()}</Text>
                    <View><Ionicons name={'md-add-circle'} size={128}/></View>
                </TouchableOpacity>
            </View>
        </View>
      );
    }
    else{
        let image = `data:image/png;base64, ${ photo.base64 }`;
        return(
            <View style={ camStyles.wrapper }>
                <Image style={camStyles.img} source={{ uri: image }}/>
            </View>
        );
    }
  }
}

const camStyles = StyleSheet.create({
    cam: {
        flex: 1,
    },
    wrapper: {
        marginTop: sbHeight * 2,
        borderRadius: 32,
        overflow: 'hidden',
        alignSelf: 'center',
        width: Dimensions.get('window').width - 10,
        height: Dimensions.get('window').width*4/3 - 10,
    },
    bottomStuff:{
        flexDirection: 'row',
        width: '100%',
        height: '100%'
    },
    bottomLocation:{
        marginTop: 20,
        alignSelf:'flex-start',
        width: '50%',
        textAlign: 'center',
    },
    // bottomButton:{
    // },
    img:{
        width: Dimensions.get('window').width - 10,
        height: Dimensions.get('window').width*4/3 - 10,
    }
})
