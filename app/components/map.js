import { Location, AppLoading, Permissions, MapView } from 'expo';
import React from 'react';
import { StatusBar, StyleSheet, Text, View, TouchableOpacity, Dimensions, ScrollView } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

const sbHeight = StatusBar.currentHeight?StatusBar.currentHeight:16;

export default class Map extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      region: null,
      mapState: 'circle', //states: circle, fullscreen, hidden
    };
  }
  

  async componentDidMount() {
    const response = await Permissions.askAsync(Permissions.LOCATION);
    if (response.status !== 'granted') throw "rerror";
    let location = await Location.getCurrentPositionAsync({});
    let region = { 
      latitude: location.coords.latitude,
      longitude: location.coords.longitude,
      latitudeDelta: 0.01,
      longitudeDelta: 0.01
      }
      this.setState({region: region})
    // this.setState({ loading: false });
  }

  renderBackButton = () => (
    <TouchableOpacity
    style={buttonStyle.back}
    onPress={() => this.setState({mapState: 'circle'}) }>
      <Text style={buttonStyle.font}>Wróć</Text>
    </TouchableOpacity>
  );

  // renderDownButton = () => (
  // <TouchableOpacity onPress={ () => this.setState({mapState: 'fullscreen'})} style={{zIndex: 1}}>
  // <Ionicons
  //   name="ios-arrow-down"
  //   size={32}
  //   color="blue"
  //   style={{position: 'absolute', left: 10, zIndex: 1, bottom: 10}}/>
  //   </TouchableOpacity>
  // );
  // renderUpButton = () => (
  // <Ionicons
  //   name="ios-arrow-up"
  //   size={32}
  //   color="blue"
  //   style={{position: 'absolute', right: 10, zIndex: 1, bottom: 10}}/>
  // );


  renderMapView = () => {
    if (this.state.mapState === 'circle'){
      return(
      <View style={ mapStyle.squareWrapper }>
      {/* {this.renderDownButton()}
        {this.renderUpButton()} */}
        <TouchableOpacity
          style={mapStyle.roundWrapper}
          onPress={ () => this.setState({mapState: 'fullscreen'})}
          >
          <MapView style={mapStyle.mapDefault}
            initialRegion={this.state.region}
            showsUserLocation={true}
            followsUserLocation={true}
            showsCompass={false}
            scrollEnabled={false}
            zoomEnabled={false}
            pitchEnabled={false}
            toolbarEnabled={false}
            />
        </TouchableOpacity>
      </View>
    )}else if(this.state.mapState === 'fullscreen'){
      return(
        <View style={mapStyle.wrapper}>
        <MapView style={mapStyle.mapFull}
        initialRegion={this.state.region}
        showsUserLocation={true}
        followsUserLocation={false}
        showsCompass={true}
        scrollEnabled={true}
        zoomEnabled={true}
        pitchEnabled={false}
        toolbarEnabled={true}
        />
        {this.renderBackButton()}
      </View>
    )}
  };

  render(){
    return (
      <View>{this.renderMapView()}</View>
    );l
    };
  }

  const mapStyle = StyleSheet.create({
    wrapper: {
      width: '100%',
      height: '100%',
      flexDirection: 'column',
    },
    squareWrapper: {
      marginTop: sbHeight + 10,
      marginBottom: 10,
      alignSelf: 'center',
      width: Dimensions.get('window').width - 10,
      height: Dimensions.get('window').width - 10,
    },
    roundWrapper:{
      borderRadius: Dimensions.get('window').width/2,
      overflow: 'hidden',
    },
    mapDefault: {
      width: '100%',
      height: '100%',
    },
    mapFull: {
      width: '100%',
      height: '90%',
    }
  })
  const buttonStyle = StyleSheet.create({
    back: {
      backgroundColor: 'blue',
      width: '100%',
      height: '10%',
    },
    font: {
      color: 'white',
      fontWeight: 'bold',
      fontSize: 20,
      textAlign: 'center',
    }
  })