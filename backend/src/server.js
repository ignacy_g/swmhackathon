const {GraphQLServer} = require('graphql-yoga');
const {Prisma} = require('prisma-binding');

const resolvers = {
    Query: {
        getTrash: (root, args, context, info) => {
            return context.db.query.trashes({}, info)
        },
    },
    Mutation: {
        newTrash: (root, args, context, info) => {
            return context.db.mutation.createTrash({
                data: {
                    lat: args.lat,
                    long: args.long,
                    desc: args.desc,
                    picB64: args.picB64,
                },
            }, info)
        }
    }
}
const server = new GraphQLServer({
    typeDefs: './src/schema.graphql',
    resolvers,
    context: req => ({
        ...req,
        db: new Prisma({
            typeDefs: 'src/generated/prisma.graphql',
            endpoint: 'https://eu1.prisma.sh/public-luminouscentaur-517/trashSpottingDB/dev',
            secret: 'braciaPierdolecJakJaIchKurwa',
            debug: true,
        }),
    }),
});
server.start(() => console.log(`Server is running on localhost:4000`));